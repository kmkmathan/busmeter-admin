$(document).ready(function(){

	
	var optionsList = [];
	var optionName = [];

	var table = $('#offersTable').DataTable({
        "ajax" : "/offersDetails",
        
        "columns": [
        	{ "data": "_id","title" : "id", "visible" : false},
        	{ "data": "amount" , "title" : "amount" },
            { "data": "minAmount" , "title" : "minAmount" },
            { "data": "origin" , "title" : "Origin" },
            { "data": "destination" , "title" : "Destination" },
            { "data": "couponCode" , "title" : "Coupon Code" },
            { "data": "expiryDate", "title" : "Expiry Date"},
            { "data": "site", "title" : "site" },
            { "data": "verifiedDate" , "title" : "Verified Date" },
            { "data": "title" , "title" : "title" },
            { "data": "offerTitle" , "title" : "Offer Title" },
            { "data": "discountType" , "title" : "Discount Type" },
            { "data": "discountTop" , "title" : "Discount Top" },
            { "data": "discountBottom" , "title" : "Discount Bottom" },
            { "data": "maximumDiscount" , "title" : "Maximum Discount" },
            { "data": "description" , "title" : "Description" },
            { "data": "isExpired" , "title" : "isExpired" },
            { "data": "isDeal" , "title" : "isDeal" },
            { "data": "isOneTime" , "title" : "isOneTime" },
            { "data": "isExpirable" , "title" : "isExpirable" },
            { "data": "isPercentage" , "title" : "isPercentage" },
            { "data": "isFlat" , "title" : "isFlat" },
            { "data": "isCashback" , "title" : "isCashback" },
            { "data": "isUpto" , "title" : "isUpto" },
            { "data": "isActive" , "title" : "isActive" }
		],
        responsive: true,
        autoWidth : false
    });

	var httpRequest = function (data)
	{
		var request = $.ajax(
		{
		  	url: "/saveOffer",
		  	contentType: "application/json",
		  	type: "post",
		  	data: JSON.stringify(data),
		  	dataType: "json"
		});
		request.done(function( msg ) 
		{
			$(':input').val('');
			$('input:checkbox').removeAttr('checked');
			table.ajax.reload();
			if(msg.message){
			    openErrorModal(response.message);
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			if(textStatus){
				var errorMsg = JSON.parse(jqXHR.responseText);
				openErrorModal(errorMsg.message);
			}
			$(':input').val('');
			$('input:checkbox').removeAttr('checked');
			table.ajax.reload();
		});
	};

	var httpUpdateRequest = function(data){
		var request = $.ajax({
		  	url: "/updateOffer",
		  	contentType: "application/json",
		  	type: "post",
		  	data: JSON.stringify(data),
		  	dataType: "json"
		});
		request.done(function(msg){
			$(':input').val('');
			$('input:checkbox').removeAttr('checked');
			table.ajax.reload();
			if(msg.message){
			    openErrorModal(response.message);
			}
		});
		request.fail(function(jqXHR, textStatus ){
			if(textStatus){
				var errorMsg = JSON.parse(jqXHR.responseText);
				openErrorModal(errorMsg.message);
			}
			$(':input').val('');
			$('input:checkbox').removeAttr('checked');
			table.ajax.reload();
		});
	};

	var openErrorModal = function(data){
		$('#errorModal').find('#errorMsg').html(data);
		$('#errorModal').modal("show");
	};

	$('#offersTable tbody').on('click', 'tr', function() {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $("#updateOfferBtn").removeAttr("data-toggle","modal");
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            $("#updateOfferBtn").attr("data-toggle","modal");
        }
    });

    $("#addOfferBtn").click(function()
    {
    	$(':input').val('');
    	table.$('tr.selected').removeClass('selected');
    	$("#updateOfferBtn").removeAttr("data-toggle","modal");
    	$("#updateOfferTitle").hide();
    	$("#updateOffer").hide();
    	$("#addOfferTitle").show();
    	$("#saveOffer").show();
    });

    $("#updateOfferBtn").click(function()
    {
    	$("#addOfferTitle").hide();
    	$("#saveOffer").hide();
    	$("#updateOfferTitle").show();
    	$("#updateOffer").show();
    	$("#updateOffer").removeAttr("data-dismiss","modal");
    });

    $("#saveOffer").click(function(e)
	{
		var expiryDate = $("#expiryDate").val();
		var site = $("#site").val();
		var isExpired = $("#isExpired").is(":checked");
		var verifiedDate = $("#verifiedDate").val();
		var couponCode = $("#couponCode").val();
		var isDeal = $("#isDeal").is(":checked");
		var isOneTime = $("#isOneTime").is(":checked");
		var isExpirable = $("#isExpirable").is(":checked");
		var title = $("#title").val();
		var description = $("#description").val();
		var offerTitle = $("#offerTitle").val();
		var discountType = $("#discountType").val();
		var discountTop = $("#discountTop").val();
		var discountBottom = $("#discountBottom").val();
		var maximumDiscount = $("#maximumDiscount").val();
		var isPercentage = $("#isPercentage").is(":checked");
		var isFlat = $("#isFlat").is(":checked");
		var isCashback = $("#isCashback").is(":checked");
		var isUpto = $("#isUpto").is(":checked");
		var isActive = $("#isActive").is(":checked");
		var amount = $("#amount").val();
		var minAmount = $("#minAmount").val();
		var origin = $("#origin").val();
		var destination = $("#destination").val();

		if(expiryDate&&couponCode&&site&&verifiedDate&&title&&offerTitle&&discountType&&discountTop&&discountBottom&&amount&&minAmount)
		{
			$("#saveOffer").attr("data-dismiss","modal");
			var data = {};
			data.expiryDate = expiryDate;
			data.site = site;
			data.isExpired = isExpired;
			data.verifiedDate = verifiedDate;
			data.couponCode = couponCode;
			data.isDeal = isDeal;
			data.isOneTime = isOneTime;
			data.isExpirable = isExpirable;
			data.title = title;
			data.description = description;
			data.offerTitle = offerTitle;
			data.discountType = discountType;
			data.discountTop = discountTop;
			data.discountBottom = discountBottom;
			data.maximumDiscount = maximumDiscount;
			data.isPercentage = isPercentage;
			data.isFlat = isFlat;
			data.isCashback = isCashback;
			data.isUpto = isUpto;
			data.isActive = isActive;
			data.amount = amount;
			data.minAmount = minAmount;
			data.origin = origin;
			data.destination = destination;
			event.preventDefault();
			httpRequest(data);
		}
		return;
	});
	


    $('#updateOfferBtn').click(function() {
    	if($(".selected").is("tr"))
    	{
	        var row = table.rows('.selected').data();
	        var row = row["0"];
	        $("#expiryDate").val(row.expiryDate);
	        $('#site option').filter(function() { 
        		return ($(this).text() == row.site); 
   				}).prop('selected', true);
			$("#isExpired").prop("checked",row.isExpired);
			$("#verifiedDate").val(row.verifiedDate);
			$("#couponCode").val(row.couponCode);
			$('#isDeal').prop('checked',row.isDeal);
			$('#isOneTime').prop('checked',row.isOneTime);
			$("#isExpirable").prop("checked",row.isExpirable);
			$("#title").val(row.title);
			$("#description").val(row.description);
			$("#offerTitle").val(row.offerTitle);
			$("#discountType").val(row.discountType);
			$("#discountTop").val(row.discountTop);
			$("#discountBottom").val(row.discountBottom);
			$("#maximumDiscount").val(row.maximumDiscount);
			$("#isPercentage").prop("checked",row.isPercentage);
			$("#isFlat").prop("checked",row.isFlat);
			$("#isCashback").prop("checked",row.isCashback);
			$("#isUpto").prop("checked",row.isUpto);
			$("#isActive").prop("checked",row.isActive);
			$("#amount").val(row.amount);
			$("#minAmount").val(row.minAmount);
			$("#origin").val(row.origin);
			$("#destination").val(row.destination);
		}
		return;
 	 });

	 $('#updateOffer').click(function(){
		 var row = table.rows('.selected').data();
        if(row && row["0"])
        {
	        var expiryDate = $("#expiryDate").val();
			var site = $("#site").val();
			var isExpired = $("#isExpired").is(":checked");
			var verifiedDate = $("#verifiedDate").val();
			var couponCode = $("#couponCode").val();
			var isDeal = $("#isDeal").is(":checked");
			var isOneTime = $("#isOneTime").is(":checked");
			var isExpirable = $("#isExpirable").is(":checked");
			var title = $("#title").val();
			var description = $("#description").val();
			var offerTitle = $("#offerTitle").val();
			var discountType = $("#discountType").val();
			var discountTop = $("#discountTop").val();
			var discountBottom = $("#discountBottom").val();
			var maximumDiscount = $("#maximumDiscount").val();
			var isPercentage = $("#isPercentage").is(":checked");
			var isFlat = $("#isFlat").is(":checked");
			var isCashback = $("#isCashback").is(":checked");
			var isUpto = $("#isUpto").is(":checked");
			var isActive = $("#isActive").is(":checked");
			var amount = $("#amount").val();
			var minAmount = $("#minAmount").val();
			var origin = $("#origin").val();
			var destination = $("#destination").val();
			if(expiryDate&&couponCode&&site&&verifiedDate&&title&&offerTitle&&discountType&&discountTop&&discountBottom&&amount&&minAmount)
			{
				$("#updateOffer").attr("data-dismiss","modal");
				var data = {};
				data.id = row["0"]._id;
				data.expiryDate = expiryDate;
				data.site = site;
				data.isExpired = isExpired;
				data.verifiedDate = verifiedDate;
				data.couponCode = couponCode;
				data.isDeal = isDeal;
				data.isOneTime = isOneTime;
				data.isExpirable = isExpirable;
				data.title = title;
				data.description = description;
				data.offerTitle = offerTitle;
				data.discountType = discountType;
				data.discountTop = discountTop;
				data.discountBottom = discountBottom;
				data.maximumDiscount = maximumDiscount;
				data.isPercentage = isPercentage;
				data.isFlat = isFlat;
				data.isCashback = isCashback;
				data.isUpto = isUpto;
				data.isActive = isActive;
				data.amount = amount;
				data.minAmount = minAmount;
				data.origin = origin;
				data.destination = destination;
				event.preventDefault();
				httpUpdateRequest(data);
			}
			return;  
        }
       return;
	});

	$('#delete').click(function() {
		$("#updateOfferBtn").removeAttr("data-toggle","modal");
        var row = table.rows('.selected').data();
        if(row && row["0"]){
	        $.ajax({
			    url: "/deleteOffer",
			    data: {"id": row["0"]._id},
			    success: function(response) {
			    	table.ajax.reload();
			    },
			    error: function(xhr) {
			    	if(xhr){
						var errorMsg = JSON.parse(xhr.responseText);
						openErrorModal(errorMsg.message);
					}
			        table.ajax.reload();
			    }
			});
        }
        return;
	});

	$("#pullOffers").click(function(){
    	$.ajax({
		    url: "/pullOffers",
		    success: function(response) {
		    	table.ajax.reload();
		    },
		    error: function(xhr) {
		    	if(xhr){
					var errorMsg = JSON.parse(xhr.responseText);
					openErrorModal(errorMsg.message);
				}
		        table.ajax.reload();
		    }
		});
    });
});