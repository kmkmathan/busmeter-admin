var app = require('../app');
var adminController = require("../server/controllers/adminController.js");

app.set('port',process.env.PORT||3000);

var server = app.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + server.address().port);
});

process.on('uncaughtException', function (err) {
  	adminController.sendMail(err);
  	console.log(err);
});