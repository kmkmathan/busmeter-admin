var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var OffersDetail = new Schema({
    expiryDate: {type : String, default: "not defined"},
    site: {type :String, default : null},
    verifiedDate: {type :String, default : null},
    couponCode: {type :String, default : null},
    amount: {type :Number, default : null},
    origin: {type :String, default : "all"},
    destination: {type :String, default : "all"},
    title: {type :String, default : null},
    description: {type :String, default : null},
    offerTitle: {type :String, default : null},
    discountType: {type :String, default : null},
    discountTop: {type :String, default : null},
    discountBottom: {type :String, default : null},
    minAmount: {type: Number, default : 0},
    maximumDiscount: {type :String, default : 0},
    isPercentage: {type : Boolean, default : false},
    isFlat: {type : Boolean, default : false},
    isCashback: {type : Boolean, default : false},
    isUpto: {type : Boolean, default : false},
    isActive: {type : Boolean, default : false},
    isExpired: {type : Boolean, default : false},
    isDeal: {type : Boolean, default : false},
    isOneTime: {type : Boolean, default : false},
    isExpirable: {type : Boolean, default : false}
});

module.exports = mongoose.model('OffersDetail', OffersDetail);
