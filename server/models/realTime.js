var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var realTime = new Schema({
    realTime: String
});

module.exports = mongoose.model('realTime', realTime);