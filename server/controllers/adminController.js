var async = require('async');
var passport = require('passport');
var colors = require('colors');
var Admin = require('../models/admin');
var smtpTransport = require('nodemailer-smtp-transport');
var nodemailer = require("nodemailer");

var config = require("../config");

var transport = nodemailer.createTransport(smtpTransport({
    service: config.emailService,
    auth: {
        user: config.emailId,
        pass: config.emailPassword
    }
}));

function AdminController(){};

var isValid = function(req, cb){
	if(req.body){
		var username = req.body.username;
		var password = req.body.password;
		if(!username){
			return cb("Username is required", null);
		}
		if(!password){
			return cb("Password is required", null);
		}
		return cb(null, req);
	}
	return cb("Username and password is required", null);
};

var register = function(req, cb){
	Admin.register(new Admin({ username : req.body.username }), req.body.password, function(err, admin) {
        if (err) {
          return cb(err.message, null);
        }else{
        	return cb(null, "Account created");
        }
	});
};

AdminController.prototype.register = function(req, res){
	async.waterfall([
        isValid.bind(this, req),
        register,
    ], function (err, data) {
        if (err) {
            res.status(400);
            return res.json({data : null, message : err});
        } else {
            passport.authenticate('local')(req, res, function () {
	            res.redirect('/offers');
	        });
        }
    });
};

AdminController.prototype.sendMail = function(error, cb){

    var errorHtml = '<div><p>The following error occured please check immediately</p><p>'+error+'</p></div>'
        
    var mailOptions = {
        from: config.emailId,
        to: config.toEmail,
        subject: 'Error in busmeter',
        text: '',
        html: errorHtml
    }

    transport.sendMail(mailOptions, function (err, data) {
        if(err){
            console.log(colors.red(err));
            cb({error:"Error please try again", data:null});
        }else{
            cb({error:null, data:"send successfully"});
        }
    });
}

module.exports = function () {
	return new AdminController();
}();
