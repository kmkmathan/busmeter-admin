var async = require('async');
var OffersDetail = require('../models/offersDetail');
var request = require('request');
var zlib = require('zlib');
var colors = require('colors');
var config = require("../config");


function offersController(){}

var isValidOffer = function(req, cb){
	if(req && req.body && req.body.couponCode)
	{
		OffersDetail.find({couponCode : req.body.couponCode} , function(err, result){
			if(!result.length)
			{
				return cb(null, req);
			}else
			{
				return cb("offer already exist", null);
			}
		});
	}else
	{
		return cb("Empty body", null);
	}
	
};

var saveOffer = function(req, cb){
	var data =req.body;
	var offersDetail = new OffersDetail(data);

	offersDetail.save(function(err){
		if(err){
			return cb(err, null);
		}else{
			offersController.prototype.updateOffersInRedis(function(result){
                if(result.error){
                    adminController.sendMail("Error while updating offers in redis -- on edit");
                    console.log("Error while updating offers in redis -- on edit", result.error);
                    return cb(result.error, null);
                }else{
                    console.log("Updated offers on edit", new Date(), result.data);
                    return cb(null, "saved");
                }
            });
		}
	});
	
};
var updateOffer = function(req, cb){
	var data = req.body;
	OffersDetail.update({_id:req.body.id}, data, function(err){
		if(err){
			return cb(err, null);
		}else{
			offersController.prototype.updateOffersInRedis(function(result){
                if(result.error){
                    adminController.sendMail("Error while updating offers in redis -- on edit");
                    console.log("Error while updating offers in redis -- on edit", result.error);
                    return cb(result.error, null);
                }else{
                    console.log("Updated offers on edit", new Date(), result.data);
                    return cb(null, "saved");
                }
            });
			
		}
	});
	
};

var requestWithEncoding = function(callback) {
	var headers = {
		"accept-charset" : "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
		"accept-language" : "en-US,en;q=0.8",
		"accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
		"user-agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
		"accept-encoding" : "gzip, deflate, sdch",
		'X-Requested-With': "XMLHttpRequest"
	};

	var options = {
		url: "http://www.coupondunia.in/category/travel/bus?coupon_type%5B%5D=2&subcategories%5B%5D=8&online_stores%5B%5D=351&online_stores%5B%5D=163&online_stores%5B%5D=159&online_stores%5B%5D=344&active_tab=online&vertical%5B%5D=0&index_name=online_offers&doAppend=false&pageNo=1&sort=Popularity&page_count=15&template=mix_horizontal_cards&action=filter_data",
		headers: headers
	};

  	var req = request.get(options);

	req.on('response', function(res) {

	    var chunks = [];

	    res.on('data', function(chunk) {
			chunks.push(chunk);
	    });

	    res.on('end', function() {
			var buffer = Buffer.concat(chunks);
			var encoding = res.headers['content-encoding'];
			if (encoding == 'gzip') {
			zlib.gunzip(buffer, function(err, decoded) {
				callback(err, decoded && decoded.toString());
			});
			} else if (encoding == 'deflate') {
			zlib.inflate(buffer, function(err, decoded) {
				callback(err, decoded && decoded.toString());
			})
			} else {
				callback(null, buffer.toString());
			}
	    });

	});

	req.on('error', function(err) {
		callback(err);
	});
}


offersController.prototype.pullOffers = function(cb){

	requestWithEncoding(function(err, data) {
		if (err){
			return cb({error : err, data : null});
		}else{
			var parsedData = null;
			try{
				parsedData = JSON.parse(data.trim());
				parsedData = parsedData.data;
			}catch(e){
				console.log("Error while parsing offers", e);
			}
			if(parsedData && parsedData.online_offers){
				var offers = [];
				parsedData.online_offers.forEach(function(offer){
					if(!offer.is_expired && offer.is_deal === '0'){
						var obj = {};
						obj.expiryDate = offer.expiry_date;
						obj.site = offer.brand_name;
						obj.isExpired = offer.is_expired;
						obj.verifiedDate = offer.verified_date;
						obj.couponCode = offer.coupon_code;
						obj.isDeal = offer.is_deal === '1' ? true : false;
						obj.isOneTime = offer.is_one_time === '1' ? true :false;
						obj.isExpirable = offer.is_expirable;
						obj.title = offer.title;
						obj.description = offer.description;
						obj.offerTitle = offer.discount_description ? offer.discount_description.replace("|", " ").toLowerCase() : "No offer";
						obj.discountType = offer.discount_type;
						obj.discountTop = offer.discount_top_phrase;
						obj.discountBottom = offer.discount_bottom_phrase;

						if(offer.discount_description.indexOf("%") !== -1){
							obj.isPercentage = true;
						}else{
							obj.isPercentage = false;
						}

						if(offer.discount_description && offer.discount_description.toLowerCase().indexOf("flat") !== -1){
							obj.isFlat = true;
						}else{
							obj.isFlat = false;
						}

						if(offer.discount_description && offer.discount_description.toLowerCase().indexOf("cashback") !== -1){
							obj.isCashback = true;
						}else{
							obj.isCashback = false;
						}

						if(offer.discount_description && offer.discount_description.toLowerCase().indexOf("upto") !== -1){
							obj.isUpto = true;
						}else{
							obj.isUpto = false;
						}
						obj.isActive = true;
						if(offer.discount_description){
							var amount = offer.discount_description.replace(/^\D+/g, '');
							obj.amount = parseInt(amount);
						}

						offers.push(obj);
					}
				});
				console.log(colors.green("Offers pulled", offers.length));
				OffersDetail.remove(function() {
			        OffersDetail.collection.insert(offers, function(err, result){
			        	if(err){
			        		return cb({error:err, data: null});
			        	}else{
			        		return cb({error:null, data: offers.length});
			        	}
					});
			    });
				
			}else{
				return cb({error: "Offers are empty or error occured while pulling offers", data: null});
			}
		}
	});
};


offersController.prototype.login = function(req, res){
	res.statusCode = 200;
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.render('login');
};

offersController.prototype.offers = function(req, res){
	res.statusCode = 200;
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.render('offers', {user: req.user});
};

offersController.prototype.realTime = function(req, res){
	res.statusCode = 200;
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.render('realTime', {user: req.user});
};


offersController.prototype.saveOffer = function(req, res){
	async.waterfall([
        isValidOffer.bind(this, req),
        saveOffer,
    ], function (err, data) {
        if (err) {
            res.status(400);
            return res.json({data : null, message : err});
        } else {
            res.status(200);
            return res.json({data : data, message : null});
        }
   });
};

offersController.prototype.offersDetails = function(req, res){

	OffersDetail.find({}, function(err, result){
		if(err){
			return res.json({data:null, message:err});
		}else{
			return res.json({data : result, message: null});
		}
	});

};

offersController.prototype.updateOffersInRedis = function(cb){
	OffersDetail.find({isExpired :  false, isDeal : false, isActive : true}, {_id:0, __v : 0}, function(err, result){
		if(err){
			return cb({error:err, data : null});
		}else{
			console.log(result);
			var rb = [];
			var paytm = [];
			var travelyaari = [];
			var goibibo = [];
			result.forEach(function(obj){
				switch(obj.site){
					case "redBus":
						rb.push(obj);
						break;
					case "Paytm":
						paytm.push(obj);
						break;
					case "goibibo":
						goibibo.push(obj);
						break;
					case "Travelyaari":
						travelyaari.push(obj);
						break;
				}
			});
			config.redisClient.set("redbus", JSON.stringify(rb));
			config.redisClient.set("paytm", JSON.stringify(paytm));
			config.redisClient.set("goibibo", JSON.stringify(goibibo));
			config.redisClient.set("travelyaari", JSON.stringify(travelyaari));
			return cb({error: null, data: "Offers updated in redis"});
		}
	});
};

offersController.prototype.deleteOffer = function(req, res){
	OffersDetail.remove({_id:req.query.id}, function(err){
		if(err){
			return res.json({data:null, message:err});
		}else{
			offersController.prototype.updateOffersInRedis(function(result){
                if(result.error){
                    adminController.sendMail("Error while updating offers in redis -- on edit");
                    console.log("Error while updating offers in redis -- on edit", result.error);
                    return res.json({data:null, message:result.error});
                }else{
                    console.log("Updated offers on edit", new Date(), result.data);
                    return res.json({data:"removed", message:null});
                }
            });
		}
	});
};

offersController.prototype.updateOffer = function(req, res){
	async.waterfall([
        updateOffer.bind(this, req),
    ], function (err, data) {
        if (err) {
            res.status(400);
            return res.json({data : null, message : err});
        } else {
            res.status(200);
            return res.json({data : data, message : null});
        }
   });
};
module.exports = function () {
	return new offersController();
}();
