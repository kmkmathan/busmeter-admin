var async = require('async');
var realTime = require('../models/realTime');

function realTimeController(){}

realTimeController.prototype.realTime = function(req, res){
	res.statusCode = 200;
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.render('realTime', {user: req.user});
};

module.exports = function () {
	return new realTimeController();
}();
