var express = require('express');
var router = express.Router();
var offersController = require("../controllers/offersController.js");
var realTimeController = require("../controllers/realTimeController.js");
var adminController = require("../controllers/adminController.js");
var CronJob = require('cron').CronJob;
var passport = require('passport');
var config = require("../config");

var isAuthenticated = function(req, res, next) {
    if (req.isAuthenticated()){
        return next();
     }
    res.redirect('/');
 };

router.get('/', function (req, res) {
     offersController.login(req, res);
});

/* router.get('/register', function(req, res) {
     res.render('register', { });
 });*/

router.get('/offers', isAuthenticated, function (req, res) {
    offersController.offers(req, res);
});

router.get('/pullOffers', isAuthenticated, function (req, res) {
    offersController.pullOffers(function(data){
        if(data.error){
            adminController.sendMail(data.error);
            console.log("Error while updating offers", data.error);
            res.status = 400;
            return res.json({message : data.error, data:null});
        }else{
            offersController.updateOffersInRedis(function(result){
                if(result.error){
                    adminController.sendMail("Error while updating offers in redis");
                    console.log("Error while updating offers in redis", result.error);
                    res.status = 400;
                    return res.json({message : result.error, data:null});
                }else{
                    console.log("Updated offers", new Date(), result.data);
                    res.status = 200;
                    return res.json({message : null, data:result.data});
                }
            });
        }
    });
});

router.get('/realTime', isAuthenticated, function (req, res) {
    realTimeController.realTime(req, res);
});

router.get('/offersDetails', isAuthenticated, function(req, res) {
    offersController.offersDetails(req, res);
});

router.get('/deleteOffer', isAuthenticated, function(req, res) {
    offersController.deleteOffer(req, res);
});
router.post('/updateOffer', isAuthenticated, function(req, res) {
    offersController.updateOffer(req, res);
});

router.get('/realTime', isAuthenticated, function(req, res) {
    realTimeController.realTime(req, res);
});

router.get('/logout', isAuthenticated, function(req, res) {
    req.logout();
    res.redirect('/');
});

/*router.post('/register', function(req, res) {
  	adminController.register(req, res);
 });*/

router.post('/saveOffer', isAuthenticated, function(req, res) {
    offersController.saveOffer(req, res);
});

router.post('/login', 
	passport.authenticate('local', 
	{

		successRedirect: '/offers',
	    failureRedirect: '/',
		failureFlash: true 
	})
);

/*new CronJob(config.cronTime, function() {
    console.log("Started -- CRON for pulling offers: ", new Date());
    offersController.pullOffers(function(data){
        if(data.error){
            adminController.sendMail(data.error);
            console.log("Error while updating offers -- CRON", data.error);
        }else{
            offersController.updateOffersInRedis(function(result){
                if(result.error){
                    adminController.sendMail("Error while updating offers in redis -- CRON");
                    console.log("Error while updating offers in redis -- CRON", result.error);
                }else{
                    console.log("Updated offers -- CRON", new Date(), result.data);
                }
            });
        }
    });
}, null, true);*/

module.exports = router;

